thickness		= 1;
pole_length		= 10;
pole_radius		= 2.5;
sphere_radius	= 4;
$fs=1;

module mount(){
	translate([0,thickness/2,1]) cube ([ 10,thickness/2,12 ],center=true);
	translate([0,0,0]) cube ([10,thickness,10],true);
	translate([0,-thickness/2,-1]) cube ([ 10,thickness/2,12 ],center=true);
}
module pole(){
	rotate([90,0,0]) cylinder(h=pole_length, r=pole_radius);
}
module swivel_ball(){
	translate ([0,-(pole_length+(sphere_radius-thickness)),0])sphere(sphere_radius);
}
mount();
pole();
swivel_ball();