thickness	=	1;
depth		=	14;
width		=	87;
height		=	162;
clip_depth	=	2.5;
support_width =	10;
module phone_shape(){
	cube(size=[width,depth,height],center=true);
}
module latch_bevel(){
	// the bottom latch
	translate([0, depth/2-thickness, -(height/2-thickness)])
		difference(){
			cube(size=[support_width,thickness,clip_depth], center=true);
			translate ([0,clip_depth/1.5,clip_depth/1.5])
				rotate ([45,0,0])
				cube(size=[support_width+thickness,clip_depth*1.75,clip_depth], center=true);
		}
	// right hand side one
	translate( [-(width/2-thickness), depth/2-thickness,0] )rotate([0,90,0])
		difference(){
			cube(size=[support_width,thickness,clip_depth], center=true);
			translate ([0,clip_depth/1.5,clip_depth/1.5])
				rotate ([45,0,0])
				cube(size=[support_width+thickness,clip_depth*1.75,clip_depth], center=true);
		}
	translate( [(width/2-thickness), depth/2-thickness,0] )rotate([0,270,0])
		difference(){
			cube(size=[support_width,thickness,clip_depth], center=true);
			translate ([0,clip_depth/1.5,clip_depth/1.5])
				rotate ([45,0,0])
				cube(size=[support_width+thickness,clip_depth*1.75,clip_depth], center=true);
		}
	rotate([0,180,0]) translate([0, depth/2-thickness, -(height/2-thickness)])
		difference(){
			cube(size=[support_width,thickness,clip_depth], center=true);
			translate ([0,clip_depth/1.5,clip_depth/1.5])
				rotate ([45,0,0])
				cube(size=[support_width+thickness,clip_depth*1.75,clip_depth], center=true);
		}
}
module extra_plastic(){
	translate([(width/4)+support_width/2, 0, (height/4)+support_width/2])
		cube(size=[(width/2)+thickness,depth*2, height/2+thickness], center=true);
	translate([-((width/4)+support_width/2), 0, -((height/4)+support_width/2)])
		cube(size=[(width/2)+thickness,depth*2, height/2+thickness], center=true);
	translate([-((width/4)+support_width/2), 0, (height/4)+support_width/2])
		cube(size=[(width/2)+thickness,depth*2, height/2+thickness], center=true);
	translate([(width/4)+support_width/2, 0, -((height/4)+support_width/2)])
		cube(size=[(width/2)+thickness,depth*2, height/2+thickness], center=true);
}
module center_extra_support(){
	translate ([ 0, -(depth/2+thickness),0])
		rotate([90,0,0])
		cylinder(d=width/2,h=thickness,center=true);
}
//additions
latch_bevel();
center_extra_support();

//subtractions
difference(){
	//an outer shape from which to carve the mount
	translate ([0,-thickness,0])
		resize([width+thickness*2, depth+thickness, height+thickness*2])
		cube (size=[width, depth, height],center=true);
	phone_shape();
	extra_plastic();
}