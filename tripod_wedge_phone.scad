$fs = 1;
base_width		= 51;
top_width		= 42;
depth			= 45;
height			= 10;
base			= 0;
wall_thickness	= 3;
phone_height	= 162;
support_diameter= 6;
support_radius	= (support_diameter/2);
sphere_radius	= 4;
thickness		= 1;
module edge_cut (side) { 
	if (side == 1){
		translate ([top_width/2+height/2,0,-.1]) cube ( size = [height+.1,base_width,height+.1], center=true);
	}
	else {
		translate ([-(top_width/2+height/2),0,-.1]) cube ( size = [height+.1,base_width,height+.1], center=true);
	}
}
module spherical_socket(){
	difference(){
		union(){	//this block contains the general shape of the mount
			translate([0,0,0]) cube ([sphere_radius*2,sphere_radius,sphere_radius*2],true);
			rotate([90,0,0]) translate([0,0,-sphere_radius/2]) cylinder(r2=sphere_radius+thickness,r1=support_radius,h=sphere_radius,center=true);
		}
		// then remove the actual spherical shape for the socket
		sphere(sphere_radius,true);
		// we need to cut out slots in the side to allow it to flex a little and allow the sphere to enter its socket
		translate([ 0,-thickness+.01,0 ]) cube([sphere_radius*3,thickness*3,thickness],center=true);
		rotate ([0,90,0]) translate ([0, -thickness+.01,0 ]) cube([sphere_radius*3, thickness*3, thickness], center=true);
		difference(){	// this block cleans up excess plastic outside the bounds of where we want it.
			cube([sphere_radius*2+5, sphere_radius, sphere_radius*2+5], center=true);
			cube([sphere_radius*2, sphere_radius, sphere_radius*2], center=true);
		}
	}
}
module mounting_wedge ( ){
	difference(){ 
		translate ([0,0,-height/2]) 
			linear_extrude(height = height, scale = top_width/base_width)
			square(base_width, center = true);
		edge_cut(1);
		edge_cut(2);
	}
}
module center_support() { 
	translate([0,0,height/2])cylinder ( h=phone_height, r=support_radius);
	translate([0,thickness*2,((height/2)+phone_height)-thickness]) rotate([90,0,0]) cylinder(h=top_width/2,r=support_radius);
	*translate([0,-top_width/2,(height/2)+phone_height-thickness]) sphere(sphere_radius);
}
module bottom_cutout () {
	translate([0,0,-wall_thickness]){
		cube ( size = [top_width-wall_thickness, depth-wall_thickness,height+wall_thickness], center=true);
	}
}
difference(){
	mounting_wedge();
	bottom_cutout();
}
center_support();
translate([ 0, -(top_width/2+(sphere_radius/2)), phone_height+height/2-thickness])spherical_socket();