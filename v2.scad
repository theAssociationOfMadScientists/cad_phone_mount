thickness	=	1.5;	//thickness of the printed parts - increase 
		//	this for added strength, decrease for cheaper prints.
depth		=	14;	//depth of the phone+case
width		=	87;	//width of the phone+case
height		=	162;//height of the phone+case
clip_depth	=	2.5;//how deep a clip will be below the top of the bracket
support_width =	10;	//width of the support beams
vent_depth	=	35;
module phone_shape(){
	cube(size=[width,depth,height],center=true);
	difference(){
		translate([0,0,0]) cube(size=[width+support_width+thickness,depth+thickness*4,height+support_width+thickness],center=true);
		cube(size=[width+thickness,depth+thickness,height+thickness],center=true);
	}
}
module latch_bevel(){
	translate([0, depth/2-thickness, -(height/2-thickness)])
		difference(){
			cube(size=[support_width,thickness,clip_depth],center=true);
			translate ([0,clip_depth/1.5,clip_depth/1.5])
				rotate ([45,0,0])
				cube(size=[support_width+thickness,clip_depth*1.75, clip_depth],center=true);
		}
}
module center_extra_support(){
	translate ([ 0, -(depth/2),0])
		rotate([90,0,0])
		cylinder(d=width/2,h=thickness);
}
module top_center_support(){
	translate([0,-thickness,height/4])
		cube(size=[support_width, depth+thickness, (height/2)+thickness],center=true);
}
module bottom_crossmember(){
	translate ([0,-thickness/2,-(height-support_width/2)-thickness]/2){
		cube ([ width+thickness, depth+thickness, support_width/2 ], center=true);
	}
	
}
module bottom_r_support(){
	rotate([ 0,270-62,0]) // change to non-hardcode value
		translate([0,-thickness/2-.001,height/4])
		cube([support_width, 
			depth+thickness, 
			(sqrt(pow((height/2),2)+pow((width/2),2))+support_width)+thickness],
			center=true);
}
module mounting_cutout(){
	translate([0,-(depth/2),1]) cube ([ 10,thickness/2,12 ],center=true);
	translate([0,-(depth/2),0]) cube (10,true);
	translate([0,-(depth/2)-thickness/2,-1]) cube ([ 10,thickness/2,12 ],center=true);
}

// assemble the modules
union(){ // here for clarity (technically redundant as default action is union)
	difference(){
		union(){
			center_extra_support();
			*top_center_support();
			mirror([1,0,0]) bottom_r_support();
			bottom_crossmember();
			bottom_r_support();
			mirror([0,0,1]) bottom_r_support();
			mirror([1,0,0]) mirror([0,0,1]) bottom_r_support();
			
		}
		phone_shape();
		mounting_cutout();
	}
		//append any mount AFTER this point, to avoid removal of sections of it,
		// due to removal of exterior shapes in module phone_shape, for cleaning up of the base frame.
	
}