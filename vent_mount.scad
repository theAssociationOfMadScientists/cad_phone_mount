thickness			= 1;
sphere_radius		= 4;
crossmember_width	= 20;
vent_mount_length	= 10;
$fs=1;	// more detailed round shapes

module spherical_socket(){
	difference(){
		union(){	//this block contains the general shape of the mount
			translate([0,0,0]) cube ([sphere_radius*2,sphere_radius,sphere_radius*2],true);
			rotate([90,0,0]) translate([0,0,-sphere_radius/2]) cylinder(r2=sphere_radius+thickness,r1=2,h=sphere_radius,center=true);
		}
		// then remove the actual spherical shape for the socket
		sphere(sphere_radius,true);
		// we need to cut out slots in the side to allow it to flex a little and allow the sphere to enter its socket
		translate([ 0,-thickness+.01,0 ]) cube([sphere_radius*3,thickness*3,thickness],center=true);
		rotate ([0,90,0]) translate ([0, -thickness+.01,0 ]) cube([sphere_radius*3, thickness*3, thickness], center=true);
		difference(){	// this block cleans up excess plastic outside the bounds of where we want it.
			cube([sphere_radius*2+5, sphere_radius, sphere_radius*2+5], center=true);
			cube([sphere_radius*2, sphere_radius, sphere_radius*2], center=true);
		}
	}
}
module crossmember(){
	translate ([0,sphere_radius+thickness/4-.001,0]) cube ([ crossmember_width, thickness/2, thickness*2],center=true);
}
module vent_clip(){
	translate([ crossmember_width/2-thickness, sphere_radius*2+thickness,0 ]) difference(){
		cube([thickness*2, vent_mount_length, thickness * 2],center=true);
		translate([ -.001, vent_mount_length/5,0 ])
			cube([thickness*2+.02, vent_mount_length, thickness] ,center=true);
	}
	
}
vent_clip();
mirror([1,0,0]) vent_clip();
spherical_socket();
crossmember();